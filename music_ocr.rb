require 'chunky_png'

TREBLE_CLEF_LINES = %w[F D B G E]
TREBLE_CLEF_SPACES = %w[G E C A F D]

module Measure
  def self.could_it_be?(feature)
    return (feature.width == 1 and feature.height == feature.staff.height)
  end
end

module Note
  def self.could_it_be?(feature)
    chunks = feature.pixels.map(&:last).chunk{|l| l.count{|c| ChunkyPNG::Color.r(c) < 100}}.map{|c,l| [c, l.size]}
    return ((chunks.size > 1) and (chunks.max_by(&:last).first == stem_width))
  end

  def self.stem_width
    # This is gonna break, obviously
    return 1
  end

  def stem_dir
    chunks = pixels.map(&:last).chunk{|l| l.count{|c| ChunkyPNG::Color.r(c) < 100}}.map{|c,l| [c, l.size]}
    largest_non_stem_chunk_index = chunks.each_with_index.reject{|c,i| c.first == stem_width}.max_by{|c,i| c.last}.last
    stem_index = chunks.each_with_index.select{|c,i| c.first == stem_width}.max_by{|c,i| c.last}.last
    return nil if largest_non_stem_chunk_index.nil? or stem_index.nil?
    return stem_index > largest_non_stem_chunk_index ? :down : :up
  end

  def stem_up?
    stem_dir == :up
  end

  def stem_down?
    stem_dir == :down
  end

  def base
    return bottom if stem_up?
    b = pixels.find{|p,r| r.count{|c| ChunkyPNG::Color.r(c) < 100} == 1}.first.last - 1
    b = b - 1 if staff.lines.include?(b)
    b
  end

  def note
    b = base + 1
    if staff.lines.include?(b) then
      note_index = staff.lines.index(b)
      return TREBLE_CLEF_SPACES[note_index]
    else
      note_index = staff.lines.each_with_index.select{|l, i| l < base}.max_by(&:first).last
      return TREBLE_CLEF_LINES[note_index]
    end
  end
end

class Feature
  attr_reader :top, :left, :bottom, :right, :staff
  def initialize(left, top, right,  bottom, staff)
    @top = top
    @left = left
    @bottom = bottom
    @right = right
    @staff = staff
    trim!
    determine!
  end

  def width; right - left; end
  def height; bottom - top; end

  def trim!
     trimmed_rows = top.upto(bottom).reject{|r| staff.lines.include?(r)}.map{|r| [r, staff.img.row(r)[left..right].map{|c| ChunkyPNG::Color.r(c) < 100}] }.select{|r, l| l.reduce(&:|)}.map(&:first)
     @top = trimmed_rows.first
     @bottom = trimmed_rows.last
     binding.pry if @top.nil? or @bottom.nil?
  end

  def pixels
    top.upto(bottom).reject{|r| staff.lines.include?(r)}.map{|r| [[left, r], staff.img.row(r)[left..right]] }
  end

  def inspect
    "#<Feature: (#{left}, #{top}) (#{right}, #{bottom})>"
  end

  def determine!
    type = [Note, Measure].find{|c| c.could_it_be?(self)}
    self.extend(type) unless type.nil?
  end
end

class Staff
  attr_reader :lines, :width, :features, :img

  def initialize(img, rows)
    @img = img
    @lines = rows
    find_start_and_width!
    find_features!
  end

  def find_start_and_width!
    chunks = @img.row(@lines.first).chunk{|i| ChunkyPNG::Color.r(i) < 100}.map{|b,l| [b, l.size]}
    max_chunk = chunks.each_with_index.max_by{|a,i| a.last}
    @width = max_chunk.first.last - 1
    @x_start = chunks.slice(0,max_chunk.last).map(&:last).reduce(&:+)
  end

  def top; @lines[0];  end
  def bottom; @lines.last; end
  def left; @x_start; end
  def right; @x_start + @width; end
  def top_left; [top, left]; end
  def top_right; [top, right]; end
  def bottom_left; [bottom, left]; end
  def bottom_right; [bottom, right]; end
  def height; bottom - top; end

  def find_features!
    # This won't work forever

      feature_coords = top.upto(bottom).reject{|i| lines.include?(i)}.map{|l| @img.row(l).map{|c| ChunkyPNG::Color.r(c) < 100}}.transpose.map{|c| c.reduce(&:|)}.each_with_index.chunk(&:first).select(&:first).map{|a,l| l.map(&:last)}
      @features = feature_coords.map{|l| Feature.new(l.first, top, l.last, bottom, self)}
  end

  def notes; features.select{|f| f.is_a?(Note)}; end
end

class Score
  attr_reader :img
  def initialize(filename)
    @img = ChunkyPNG::Image.from_file(filename)
  end

  def staves
    return @staves if @staves
    @staves = staff_line_rows.each_slice(5).map{|r| Staff.new(@img, r)}
    return @staves
  end

  alias_method :staffs, :staves

  def staff_line_rows
    return @staff_line_rows if @staff_line_rows
    l = @img.pixels.each_slice(@img.width).map{|r| r.map{|c| ChunkyPNG::Color.r(c) < 100 ? 1 : 0}.reduce(&:+) }
    m = l.max.to_f
    @staff_line_rows = l.each_with_index.select{|v,r| v.to_f / m > 0.6}.map(&:last)
    @staff_line_rows
  end
end
